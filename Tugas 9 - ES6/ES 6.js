//Soal 1.
console.log("\nMengubah Fungsi Menjadi Fungsi Arrow\n")

const golden1 = () => {
    console.log("This is Golden!!")
}
golden1()

//Soal 2.
console.log("\nSederhanakan menjadi Object Literal di ES6\n")

const newFunction = (firstName, lastName)=>{
    // const fullname = {firstName,lastName}
    const fullname = `${firstName} ${lastName}`
    console.log(fullname)
   
}

newFunction("William", "Immoh")

//Soal 3
console.log("\n Soal No 3 Destructuring \n")

let newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell}= newObject
console.log(firstName, lastName, destination, occupation, spell)

//Soal 4
console.log("\n Soal No 4 Array Spreading\n")

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

//Soal 5 
console.log("\nSoal No 5 Template Literals\n")

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(before) 