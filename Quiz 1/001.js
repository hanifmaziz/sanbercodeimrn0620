//A.
// Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
//     dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
//     maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
//     dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
//     maka function akan mereturn nilai -1. 
console.log(" Soal A.")
function pembanding(num1, num2){
    if (num1<0||num2<0)
    {
        return console.log("-1")
    }
    else if(num1==num2)
    {
        return console.log("-1")
    }
    else if(num1>num2)
    {
        return console.log(num1)
    }
    else if(num1<num2)
    {
        return console.log(num2)
    }
}

var num1 =1
var num2 =1
pembanding(num1,num2)

console.log(" Soal B.")
// B. Balik String (10 poin)
// Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

// NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
// Hanya boleh gunakan looping. 

function balikString(inputString){
    var input = inputString
  for (var i=1; i<=input.length; i++){
      var output = input[input.length-i]
      process.stdout.write(output)
  }
}

var inputString = "mantap"
balikString(inputString)

// C. Palindrome (10 poin)
//     Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
//     Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
//     Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
//     Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

  
//     NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
//     Hanya boleh gunakan looping. 

console.log("Soal C.")

function palindrome(inputPalindrome){
    var newStr=inputPalindrome
    for(var i=0; i < (newStr.length)/2; i++){ 
        if(newStr[i] == newStr[newStr.length-i-1]){ 
          return true; 
        } else 
          return false; 
      }
}

var newStr = "abcba"
console.log(palindrome(newStr))