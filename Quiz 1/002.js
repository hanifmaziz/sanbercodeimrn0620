
//   A. Descending Ten (10 poin)
//   Function DescendingTen adalah kebalikan dari function AscendingTen. 
//   Output yang diharapkan adalah deretan angka dimulai dari angka parameter hingga 10 angka di bawahnya. 
//   Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan.

//   contoh: 
//   console.log(DescendingTen(10)) akan menampilkan 10 9 8 7 6 5 4 3 2 1
//   console.log(DescendingTen(20)) akan menampilkan 20 19 18 17 16 15 14 13 12 11

//   Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number.

console.log("Soal A.")
function DescendingTen(num){
    var input = num
if (input>9){
    var inputArry = input.toString()
    for(var i=0; i<9;i++)
    {
        input--
       inputArry+=(" "+input)
    }
    console.log(inputArry)
}
else if (input<10){
    var hold1 = input
    var hold = 10-hold1
    for(var i=0; i<hold1;i++)
    {
       inputArry+=(" "+input)
       input--

    }
    for(var i=0; i<hold;i++)
    {
        inputArry+="-1"
    }
    console.log(inputArry)
}
else if(num==null){
    console.log(-1)
}
}
DescendingTen(100) // 10 9 8 7 6 5 4 3 2 1
DescendingTen(10)
DescendingTen()

console.log("\nSoal B.\n")
function AscendingTen(num){
    var input = num
    var inputArry = input.toString()
    if (num== null){
        return -1
    }
    for (var i=0; i<9; i++)
    {
        input++
        inputArry += " " +input
    }
    return inputArry
}
console.log(AscendingTen(1)) 
    console.log(AscendingTen(101)) 

    console.log("\nSoal C.\n")
    function AscDesc(num, check){
        var input = num
        var inputArry = input.toString()
    if (num=null){
        return -1
    }
    if (check%2==1){
        for (var i=0; i<9; i++)
        {
            input++
            inputArry += " " +input
        }
        return inputArry
    }
    if (check%2==0){
        for (var i=0; i<9; i++)
        {
            input--
            inputArry += " " +input
        }
        return inputArry
    }
}
console.log(AscDesc(1, 1))
console.log(AscDesc(100, 4))

console.log("\nSoal D.\n")
function ularTangga(){
    var num = 100
    var hold = ""
    for(var i =1; i<=10; i++)
    {
        if(i%2==1)
        {
            for(var j=0; j<10; j++)
            {
                hold += num + " "
                num--
            }
            console.log(hold)
            hold= ""
        }
        if(i%2==0)
        {
            for(var j=0; j<10; j++)
            {
                hold += num + " "
                num--
            }
           var split1 = hold.split(" ")
           var revers= split1.reverse()
           var joins = revers.join(" ")
            console.log(joins)
            hold= ""

        }
    }
}
ularTangga()
