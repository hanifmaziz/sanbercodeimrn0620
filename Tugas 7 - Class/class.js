//Soal No 1. Animal Class
console.log("\nSoal No 1. Animal Class\n")
//release 0
console.log("Release 0")
class Animal {
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }
}
var sheep = new Animal("shaun")
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
//release 1
console.log("\nRelease 1\n")
class Animal1 {
    constructor(nama){
        this.nama = nama;
    }
    jumlah(kaki){
        this.kaki=4
        console.log(this.kaki)
    }
}
class Ape extends Animal1{
    yell(){
        return "Auoooo"
    }
}
class Frog extends Animal1{
    jumlah(){
        super.jumlah()
        return this.jumlah
    }
    jump(){
        return "Hop Hop"
    }
}
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
console.log(sungokong.nama)
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"
kodok.jumlah()


//Soal No 2. Function To Class
console.log("\nSoal No 2. Function To Class\n")
  class Clock1{
        constructor({template}){
            this._template = template;
        }
    render (){
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this._template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    };
        start(){
            this.render();
            this.timer = setInterval(this.render.bind(this), 1000);
        };
        stop(){
            clearInterval(this.timer);
        };
  }
  var clock = new Clock1({template: 'h:m:s'});
  clock.start(); 