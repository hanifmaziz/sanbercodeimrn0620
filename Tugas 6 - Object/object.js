//Soal No 1
console.log("\n Soal No 1 - Array To Object \n")
//function
function arrayToObject(arry){
    var now = new Date()
    var thisYear = now.getFullYear()
    var people1 = {
        firstname : arry[0][0],
        lastname : arry[0][1],
        gender : arry[0][2],
        age : thisYear- arry[0][3]
       }
   
   var people2 = {
    firstname : arry[1][0],
    lastname : arry[1][1],
    gender : arry[1][2],
    age : thisYear- arry[1][3]
   }
   if (people1.age<0){
    people1.age = "Invalid Birth Year"
}

console.log("1. "+people1.firstname+" "+people1.lastname)
console.log(people1)
   if (people2.age<0){
    people2.age = "Invalid Birth Year"
}
   console.log("2. "+people2.firstname+" "+people2.lastname)
   console.log(people2)
}
//input 
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people)
arrayToObject(people2)

//Soal No 2
console.log("\n Soal No 2 - Shopping Time \n")
//function

function shoppingTime(memberId, money){
    //keterangan
    console.log("Sepatu brand Stacattu seharga 1500000")
    console.log("Baju brand Zoro seharga 500000")
    console.log("Baju brand H&N seharga 250000")
    console.log("Sweater brand Uniklooh seharga 175000")
    console.log("Casing Handphone seharga 50000")
    
    //harga
    var listItems = [
        ["Sepatu Stacattu", 1500000],
        ["Baju Brand Zoro", 500000],
        ["Baju Brand H&N", 250000],
        ["Sweater Brand Uniklooh", 175000],
        ["Casing Handphone", 50000],
        ["", 0]
    ]

    //validasi
    var i = true
    if (memberId==''){
        console.log("Mohon maaf, toko X hanya berlaku untuk member saja")
        i = false
        return false
    }
    if(money<50000)
    {
        console.log("Mohon maaf, uang tidak cukup")
        i = false
        return false
    }
    if (i==true){
        var shop = {
            memberId : memberId,
            money : money,
            listPurchased: listItems,
            changeMoney : money
        }
        if(shop.changeMoney>=listItems[0][1])
        {
            shop.listPurchased = listItems[0][0]
            shop.changeMoney=shop.changeMoney-listItems[0][1]
        
            if(shop.changeMoney>=listItems[1][1])
            {
                shop.listPurchased = listItems[1][0] +" ,"+ listItems[0][0]
                shop.changeMoney=shop.changeMoney-listItems[1][1]
                if(shop.changeMoney>=listItems[2][1])
                {
                    shop.listPurchased = listItems[1][0] +" ,"+ listItems[0][0]+" ,"+listItems[2][0]
                    shop.changeMoney=shop.changeMoney-listItems[2][1]
                    if(shop.changeMoney>=listItems[3][1])
                    {
                        shop.listPurchased = listItems[1][0] +" ,"+ listItems[0][0]+" ,"+listItems[2][0]+" ,"+listItems[3][0]
                        shop.changeMoney=shop.changeMoney-listItems[3][1]
                        if(shop.changeMoney>=listItems[4][1])
                        {
                            shop.listPurchased = listItems[1][0] +" ,"+ listItems[0][0]+" ,"+listItems[2][0]+" ,"+listItems[3][0]+" ,"+listItems[4][0]
                            shop.changeMoney=shop.changeMoney-listItems[4][1]
                            console.log(shop)
                        } else {console.log(shop)}
                    }else {console.log(shop)}
                }else {console.log(shop)}
            }else {console.log(shop)}
        }
        if(shop.changeMoney>=listItems[1][1])
            {
                shop.listPurchased = listItems[1][0] 
                shop.changeMoney=shop.changeMoney-listItems[1][1]
                if(shop.changeMoney>=listItems[2][1])
                {
                    shop.listPurchased = listItems[1][0] +" ,"+listItems[2][0]
                    shop.changeMoney=shop.changeMoney-listItems[2][1]
                    if(shop.changeMoney>=listItems[3][1])
                    {
                        shop.listPurchased = listItems[1][0] +" ,"+listItems[2][0]+" ,"+listItems[3][0]
                        shop.changeMoney=shop.changeMoney-listItems[3][1]
                        if(shop.changeMoney>=listItems[4][1])
                        {
                            shop.listPurchased = listItems[1][0] +" ,"+listItems[2][0]+" ,"+listItems[3][0]+" ,"+listItems[4][0]
                            shop.changeMoney=shop.changeMoney-listItems[4][1]
                            console.log(shop)
                        } else {console.log(shop)}
                    }else {console.log(shop)}
                }else {console.log(shop)}
            }
        if(shop.changeMoney>=listItems[2][1])
            {
                shop.listPurchased = listItems[2][0]
                shop.changeMoney=shop.changeMoney-listItems[2][1]
                if(shop.changeMoney>=listItems[3][1])
                {
                    shop.listPurchased = listItems[2][0]+" ,"+listItems[3][0]
                    shop.changeMoney=shop.changeMoney-listItems[3][1]
                    if(shop.changeMoney>=listItems[4][1])
                    {
                        shop.listPurchased = listItems[2][0]+" ,"+listItems[3][0]+" ,"+listItems[4][0]
                        shop.changeMoney=shop.changeMoney-listItems[4][1]
                        console.log(shop)
                    } else {console.log(shop)}
                }else {console.log(shop)}
            } 
        if(shop.changeMoney>=listItems[3][1])
                {
                    shop.listPurchased = listItems[3][0]
                    shop.changeMoney=shop.changeMoney-listItems[3][1]
                    if(shop.changeMoney>=listItems[4][1])
                    {
                        shop.listPurchased = listItems[3][0]+" ,"+listItems[4][0]
                        shop.changeMoney=shop.changeMoney-listItems[4][1]
                        console.log(shop)
                    } else {console.log(shop)}
                }
        if(shop.changeMoney>=listItems[4][1])
                {
                    shop.listPurchased = listItems[4][0]
                    shop.changeMoney=shop.changeMoney-listItems[4][1]
                    console.log(shop)
                }
    } 
}
var member = '1820RzKrnWn08'
var duit = 170000
shoppingTime(member,duit)


//Soal No 3
console.log("\n Soal No 3 - Naik Angkot \n")
//function
function naikAngkot(listPenumpang){
    var rute = ['A', 'B', 'C', 'D', 'E', 'F']
    for (var i=0; i<rute.length; i++)
    {
        if(listPenumpang[0][1]==rute[i])
        {
            for (var j=0; j<rute.length; j++)
            {
                if (listPenumpang[0][2]==rute[j]){
                    var jarak = i-j
                    var bayar = jarak*2000
                    console.log(listPenumpang[0][0])
                    console.log(Math.abs(bayar))
                }
            }
        }
        if(listPenumpang[1][1]==rute[i])
        {
            for (var j=0; j<rute.length; j++)
            {
                if (listPenumpang[1][2]==rute[j]){
                    var jarak = i-j
                    var bayar = jarak*2000
                    console.log(listPenumpang[1][0])
                    console.log(Math.abs(bayar))
                }
            }
        }
    }
}
var input =[['Dimitri', 'F', 'A'], ['Icha', 'A', 'B']]
naikAngkot(input)