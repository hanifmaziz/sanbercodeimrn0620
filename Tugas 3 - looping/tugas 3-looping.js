//soal no 1 looping
console.log('No. 1 Looping While \n')

var x =0
var y =20
console.log('LOOPING PERTAMA')
while (x<20)
{
    x+=2
    console.log(x + ' - I love coding')
}
console.log('LOOPING KEDUA')
while (y>0)
{
    console.log(y + ' - I will become a mobile developer')
    y-=2
}

console.log('\n')

//soal no 2 looping for
//SYARAT:
// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
console.log('No. 2 Looping menggunakan for \n')

for (var i=1; i<=20; i++)
{
    if(i%2==0)
    {
     console.log(i + ' - Berkualitas')
    }
    else if(i%3==0)
    {
        console.log(i + ' - I Love Coding')
    }
    else
    {
        console.log(i + ' - Santai')
    }
}
console.log('\n')

//Soal no 3 Membuat persegi panjang ##
console.log('No. 3 Membuat persegi panjang ## \n')
for (var i=0; i<4; i++)
{
    for (var j=0;j<7;j++)
    {
        process.stdout.write("#")
    }
    console.log()
}
console.log('\n')

//Soal no 4 Membuat persegi panjang ##
console.log('No. 4 Membuat Tangga # \n')

for(var i=1; i<=7; i++)
{
    for(var j=0; j<i; j++)
    {
        process.stdout.write("#")
    }
    console.log()
}
console.log('\n')

//Soal no 5 Membuat Papan Catur
console.log('No. 5 Membuat Papan Catur \n')
var i =0
while(i<8){
    for (var j=0; j<4; j++)
    {
        if(i%2==0){
        process.stdout.write("# ")
        }
        else{
            process.stdout.write(" #")
        }
    }
    console.log()
    i++;
}

