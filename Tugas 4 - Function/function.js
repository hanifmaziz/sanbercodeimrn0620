// Soal No 1
console.log("----Soal 1----")
function teriak(){
    // process.stdout.write("Halo Sanbers!");
    console.log("Halo Sanbers!")
}
teriak()

// Soal No 2
console.log("----Soal 2----")

function kalikan(num1, num2){
 return num1*num2
}

var num1 = 12
var num2 = 4

var hasilkali = (kalikan(num1,num2))
console.log(hasilkali)

// Soal No 3
console.log("----Soal 3----")
function introduce(name,age,address,hobby){
    console.log("Nama saya "+name+", umur"+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby)
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan= introduce(name,age,address,hobby)
introduce()