//Soal No 1 Range
console.log("\nSoal No 1 Range\n")
function range(startNum, finishNum){
if (startNum<finishNum)
    {
        var output = [startNum]
        var x = startNum
        for (var i=0; i<(finishNum-startNum); i++)
        {
            x=x+1
            output.push(x)
        }
        return output
    }
else if (finishNum<startNum)
    {
        var output = [startNum]
        var x = startNum
        for(var i =0; i<(startNum-finishNum); i++)
        {
            x=x-1
            output.push(x)
        }
        return output
    }
else {
    return -1
}
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
 console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal No 2 Range with Step
console.log("\nSoal No 2 Range With Step\n")
function rangeWithStep(startNum, finishNum, step){
    if (startNum<finishNum)
    {
        var output = [startNum]
        var x = startNum
        for (var i=0; i<(finishNum-startNum)/step; i++)
        {
            x=x+step
            if (x<finishNum){
            output.push(x)
            }
        }
        return output
    }
else if (finishNum<startNum)
    {
        var output = [startNum]
        var x = startNum
        for(var i =0; i<(startNum-finishNum)/step; i++)
        {
            x=x-step
            if (x>finishNum){
            output.push(x)
            }
        }
        return output
    }
else {
    return -1
}
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

//Soal No 3 Sum of Range
console.log("\nSoal No 3 Sum of Range\n")
function sum1(startNum, finishNum,step){
    if (step==null)
    {
        step=1
    }
    if (startNum==null)
    {
        startNum=0
    }
    if (finishNum==null)
    {
        finishNum=0
    }
    if (startNum<finishNum)
    {
        var output = [startNum]
        var x = startNum
        for (var i=0; i<(finishNum-startNum)/step; i++)
        {
            x=x+step
            if (x<finishNum &&step!=1){
            output.push(x)
            }
            if(step==1){
                output.push(x)
            }
        }
        var output1 = 0
        for (var i=0; i<output.length;i++)
        {
            output1+=output[i]
        }
        return output1
    }
else if (finishNum<startNum)
    {
        var output = [startNum]
        var x = startNum
        for(var i =0; i<(startNum-finishNum)/step; i++)
        {
            x=x-step
            if (x>finishNum & step!=1){
            output.push(x)
            }
            if(step==1){
                output.push(x)
            }
        }
        var output1 = 0
        for (var i=0; i<output.length;i++)
        {
            output1+=output[i]
        }
        return output1
    }
else {
    return (startNum+finishNum)
}
}
console.log(sum1(1,10)) // 55
console.log(sum1(5, 50, 2)) // 621
console.log(sum1(15,10)) // 75
console.log(sum1(20, 10, 2)) // 90
console.log(sum1(1)) // 1
console.log(sum1()) // 0 

//Soal no 4 Array multidimensi
console.log("\nSoal No 4 Array Multidimensi\n")
function dataHandling(inputArry){
  for (var i=0; i<inputArry.length; i++)
  {
          console.log("Nomor ID: "+inputArry[i][0])
          console.log("Nama Lengkap: "+inputArry[i][1])
          console.log("TTL: "+inputArry[i][2])
          console.log("Membaca: "+inputArry[i][3])
          console.log()
  }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 
dataHandling(input)

//Soal no 5 Balik kata
console.log("\nSoal No 5 Balik Kata\n")
function balikKata(input)
{
    var output = ""
    for (var i=1; i<input.length+1; i++)
    {
        output += input[input.length-i]
    }
    return output
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal no 6 Metode Array
console.log("\nSoal No 6 Metode Array\n")
function dataHandling2(input2){
    input2.splice(5,1,"Pria")
    input2.splice(6,0,"SMA Internation Metro")
    console.log(input2)

    var split1 = input2[3].split("/")
    console.log(split1[1])
    var split2 = split1[1]
    switch(split2){
        case 1: {console.log("Januari"); break}
        case 2: {console.log("Februari"); break}
        case 3: {console.log("Maret"); break}
        case 4: {console.log("April"); break}
        case 5: {console.log("Mei"); break}
        case 6: {console.log("Juni"); break}
        case 7: {console.log("Juli"); break}
        case 8: {console.log("Agustus"); break}
        case 9: {console.log("September"); break}
        case 10: {console.log("Oktober"); break}
        case 11: {console.log("November"); break}
        case 12: {console.log("Desember"); }
    }   
    console.log(split1)
    var add = split1.join("-")
    console.log(add)
    console.log(input[1])
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/5/1989", "Membaca"];
dataHandling2(input);
